FROM harisekhon/ubuntu-java:latest
ENV NODE_ENV=production

# Update and install zip
RUN apt-get update
RUN apt-get install -y p7zip \
    p7zip-full \
    unace \
    zip \
    unzip \
    bzip2

# Install curl and node.js
RUN apt-get install -y curl \
  && curl -sL https://deb.nodesource.com/setup_18.x | bash - \
  && apt-get install -y nodejs \
  && curl -L https://www.npmjs.com/install.sh | sh

# Version numbers
#ARG CHROME_VERSION=115.0.5790.110
ARG CHROMDRIVER_VERSION=115.0.5790.102

# Install chrome
RUN apt-get install -y wget
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \ 
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
RUN apt-get update && apt-get -y install google-chrome-stable

# Install webdriver manager and mocha
RUN npm i -g webdriver-manager
RUN curl -L webdriver-manager update | sh
RUN npm i -g mocha

# Install chromedriver
RUN wget -N https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/$CHROMDRIVER_VERSION/linux64/chromedriver-linux64.zip -P ~/
RUN unzip ~/chromedriver-linux64.zip -d ~/
RUN rm ~/chromedriver-linux64.zip
RUN mv -f ~/chromedriver-linux64 /usr/local/bin/chromedriver
RUN chown root:root /usr/local/bin/chromedriver
RUN chmod 0755 /usr/local/bin/chromedriver

WORKDIR /docker
COPY package.json package-lock.json* test.js ./

RUN npm install --production

COPY . .

CMD export PATH=$PATH:/usr/local/bin/chromedriver; npm t