# automation

## How to run tests

Now you can run tests only on your local machine. To do so, you need to do the following:
1. Install the required software;
2. git clone this repo to your local machine;
3. Install Selenium Web Driver and Mocha
4. Install dependencies
5. Set up Chromedriver
6. Run tests
6.1. Managing variables

You can find how to perform these steps below.

## 1. Installing the Required Software

- [ ] [git](https://git-scm.com/downloads)
- [ ] [Node.js](https://nodejs.org/en/download)
- [ ] [npm](https://www.npmjs.com/package/npm)
- [ ] [Java Runtime Environment](https://www.java.com/en/download/)
- [ ] [Latest Chrome Browser version](https://www.google.com/chrome/)

## 2. Clonning the repo

- Go to https://gitlab.com/south-garage.ru/automation
- Click "Clone", and copy SSH or HTTPS link (HTTPS is recommended).
- Open the terminal on your local machine, and change directory to folder where you want to keep this project.
- Clone the repo with one of the following commands:

```
git clone https://gitlab.com/south-garage.ru/automation.git
git clone git@gitlab.com:south-garage.ru/automation.git
```

## 3. Installing Selenium Web Driver and Mocha

To easily install, configure, and use Selenium Web Driver you need to install webdriver-manager package globally using npm. Enter the following commands in your terminal line by line:

```
npm i -g webdriver-manager
webdriver-manager update
```
Then you need to install mocha test framework globally. To do so, enter the following command in your terminal:

```
npm i -g mocha
```
## 4. Installing dependencies

- Open terminal, and go to the project's directory (where you cloned the repo).
- Enter the following command:

```
npm install
```

## 5. Setting up Chromedriver

To run tests you need to install Chromedriver and add its location to the system's PATH. The easiest way to install is to use the npm command:

```
npm install chromedriver
```

If you you are using Windows, find the location of the chromedriver.exe (normally it should be in: C:\Users\user\AppData\Roaming\npm\node_modules\chromedriver\lib\chromedriver\chromedriver.exe), and add the location to the system's PATH.
You can check that it's added to the PATH by entering the following command in terminal:

```
where chromedriver
```

Or you can install and add Chromedriver to the path in your convinient way.

## 6. Running tests

To run tests open the terminal, change directory to project's folder, and enter the following command:

```
npm t
```

## 6.1. Managing variables

You can change the testing domain by editing the value of 'const URL' in the 'test.js' file. This will allow you you to test the app on the local server. By default it's:

```
const URL = 'http://south-garage.ru';
```

You can change it to localhost.

Also you can change the value of 'const timeout'. This variable defines a period of time that browser are waiting for elements to appear. If the app is running slow, tests could fail due to exceeding the timeout value. By default it's:

```
const timeout = 5000;
```