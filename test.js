const { Browser, By, Key, until } = require('selenium-webdriver');
const { ignore, suite } = require('selenium-webdriver/testing');
const chai = require('chai');
const expect = chai.expect;
const URL = 'http://south-garage.ru';
//const URL = 'http://127.0.0.1:8000';
//const URL = 'http://host.docker.internal:8000';
const timeout = 15000;
const sleepTimeout = 1500;

//header
const headerUserIcon = '//i[@class="icon-user"]';
const headerBagIcon = '//i[@class="icon-bag"]';
const headerBagIconCounter = '//*[@class="item-count"]';

//cartDropdown
const cartDropdownOrderButton = '(//ul[@class="offcanvas-cart-action-button"]/li)[2]';
const cartDropdownCartButton = '(//ul[@class="offcanvas-cart-action-button"]/li)[1]';
const cartDropdownDeleteProductButton = '//i[@class="fa fa-trash-o"]';
const cartDropdownFirstProductImg = '//a[@class="offcanvas-cart-item-image-link"]';
const cartDropdownProductsWrapperPlaceholder = '//ul[@class="offcanvas-add-cart-wrapper"]/h4';

//cartPage
const cartProductName = '//td[@class="product_name"]';
const cartProductNameLink = '//td[@class="product_name"]/a';
const cartPrice = '//td[@class="product-price"]';
const cartCheckoutButton = '//div[@class="checkout_btn"]/a';

//ordersPage
const ordersEmptyCartImg = '//img[@src="/static/images/empty-cart.png"]';
const ordersLogoutButton = '(//div[@class="dashboard_tab_button aos-init aos-animate"]/ul/li)[2]';
const ordersFirstOrderNumber = '//tr[1]/td[1]//span[@class="success"]';
const ordersFirstOrderStatus = '//tr[1]/td[3]//span[@class="success"]';

//orderPage
const orderProductName = '//td[@class="product_name"]';
const orderProductNameLink = '//td[@class="product_name"]/a';
const orderSumme = '//p[@id="totalamount"]';
const orderDeliveryName = '//div[@class="cart_subtotal"][2]/p[1]';
const orderCancelButton = '//button[@class="btn btn-danger"]';

//signUpPage, registration form
const registrationForm = '//div[contains(@class, "register")]';
const registrationFormEmail = '(//div[contains(@class, "register")]//input)[2]';
const registrationFormPassword = '(//div[contains(@class, "register")]//input)[3]';
const registrationFormName = '(//div[contains(@class, "register")]//input)[4]';
const registrationFormSurname = '(//div[contains(@class, "register")]//input)[5]';
const registrationFormRegisterButton = '//div[contains(@class, "register")]//button';

//signUpPage, login form
const loginForm = '//div[(@class="account_form aos-init aos-animate")]';
const loginFormEmail = '(//div[(@class="account_form aos-init aos-animate")]//input)[2]';
const loginFormPassword = '(//div[(@class="account_form aos-init aos-animate")]//input)[3]';
const loginFormLoginButton = '//div[(@class="account_form aos-init aos-animate")]//button';
const authorizationAlert1 = '//div[(@class="alert alert-info")]';
const authorizationAlert2 = '//div[(@class="alert alert-danger")]';

//mainPage
const mainPageProductsFirstProductAddButton = '(//div[@class="col-xl-3 col-lg-4 col-sm-6 col-12"])[1]//button[@class="btn btn-md btn-black-default-hover"]';
const mainPageProductsFirstProductImg = '(//div[@class="col-xl-3 col-lg-4 col-sm-6 col-12"])[1]//a[@class="image-link"]';

//productPage
const productPageAddButton = '//div[@class="product-add-to-cart-btn"]';
const productQuantityInput = '//div[@class="product-variable-quantity"]/input';
const productPrice = '//div[@class="price"]';

//makeOrderPage
const makeOrderName = '//input[@name="first_name"]';
const makeOrderSurName = '//input[@name="last_name"]';
const makeOrderZip = '//input[@name="zip_code"]';
const makeOrderAddress = '//input[@name="address"]';
const makeOrderPhone = '//input[@name="phone"]';
const makeOrderDeliveryDropdown = '//div[@class="nice-select"]';
const makeOrderDeliveryDropdownCDEK = '(//ul[@class="list"]/li)[2]';
const makeOrderNextButton = '//div[@class="button"]/button';

async function randomNumber(min, max) {
  return Math.round((Math.random() * (max - min) + min)*1000000000);
};

const chrome = require('selenium-webdriver/chrome');
const options = new chrome.Options();
options.addArguments('--window-size=1920,1080');
options.addArguments('--disable-dev-shm-usage');
options.addArguments('--no-sandbox');
options.addArguments('--headless')
let driver

async function takeScreenshot() {
  await driver.takeScreenshot().then(
    function(image) {
      let fileName = Math.round((Math.random() * (3 - 1) + 1)*1000000000);
      require('fs').writeFileSync(`../ScreenShots/${fileName}.png`, image, 'base64');
  }
  )
}

async function register(email) {
  await driver.get(`${URL}/antananarivu-madagaskar/`);
  await driver.wait(until.elementLocated(By.xpath(registrationForm)), timeout);
  await driver.findElement(By.xpath(registrationFormEmail)).sendKeys(`${email}@sobaka.com`);
  await driver.findElement(By.xpath(registrationFormPassword)).sendKeys('12345');
  await driver.findElement(By.xpath(registrationFormName)).sendKeys('Maksim');
  await driver.findElement(By.xpath(registrationFormSurname)).sendKeys('Alekseev');
  await driver.findElement(By.xpath(registrationFormRegisterButton)).click();
}

async function login(email) {
  await driver.get(`${URL}/sign-in/`);
  await driver.wait(until.elementLocated(By.xpath(loginForm)), timeout);
  await driver.findElement(By.xpath(loginFormEmail)).sendKeys(`${email}@sobaka.com`);
  await driver.findElement(By.xpath(loginFormPassword)).sendKeys('12345');
  await driver.findElement(By.xpath(loginFormLoginButton)).click();
}

async function addProductFromMain() {
  await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductAddButton)), timeout);
  await driver.findElement(By.xpath(mainPageProductsFirstProductAddButton)).click();
  await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
  itemsCount = await driver.findElement(By.xpath(headerBagIconCounter)).getAttribute("innerText");
  expect(itemsCount).to.equal('1');
}

async function logout() {
  await driver.get(`${URL}/orders/show-orders/`);
  await driver.wait(until.elementLocated(By.xpath(ordersLogoutButton)), timeout);
  await driver.findElement(By.xpath(ordersLogoutButton)).click();
}

suite(function (env) {
  describe('Smoking tests positive scenarios', function () {

    before(async function () {
      // env.builder() returns a Builder instance preconfigured for the
      // envrionment's target browser (you may still define browser specific
      // options if necessary (i.e. firefox.Options or chrome.Options)).
      driver = await env.builder().forBrowser(Browser.CHROME).setChromeOptions(options).build();
      driver.manage().window().maximize();
    })

    it.only('Possible to register (via stub)', async function () {
      email1 = await randomNumber(1,3);
      await register(email1);
      await driver.wait(until.elementLocated(By.xpath(headerUserIcon)), timeout);
      await driver.findElement(By.xpath(headerUserIcon)).click();
      await driver.wait(until.elementLocated(By.xpath(ordersEmptyCartImg)), timeout);
      await driver.findElement(By.xpath(ordersLogoutButton)).click();
      await takeScreenshot();
    })

    it('Possible to sign in', async function () {
      await login(email1);
      await driver.wait(until.elementLocated(By.xpath(headerUserIcon)), timeout);
      await driver.findElement(By.xpath(headerUserIcon)).click();
      await driver.wait(until.elementLocated(By.xpath(ordersEmptyCartImg)), timeout);
      await driver.findElement(By.xpath(ordersLogoutButton)).click();
    })

    it('Sign in page opens when ordering unauthorized', async function () {
    await driver.get(`${URL}`);
    await addProductFromMain();
    await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
    await driver.findElement(By.xpath(headerBagIcon)).click();
    await driver.sleep(sleepTimeout);
    await driver.wait(until.elementLocated(By.xpath(cartDropdownOrderButton)), timeout);
    await driver.findElement(By.xpath(cartDropdownOrderButton)).click();
    await driver.wait(until.elementLocated(By.xpath(loginForm)), timeout);
    alertText = await driver.findElement(By.xpath(authorizationAlert1)).getAttribute("innerText");
    expect(alertText).to.equal('После авторизации вы сможете продолжить оформлять свой заказ');
    await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
    await driver.findElement(By.xpath(headerBagIcon)).click();
    await driver.sleep(sleepTimeout);
    await driver.wait(until.elementLocated(By.xpath(cartDropdownDeleteProductButton)), timeout);
    await driver.findElement(By.xpath(cartDropdownDeleteProductButton)).click();
    })

    it('Possible to add a product to the cart from the homepage', async function () {
      await driver.get(`${URL}`);
      href1 = await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).getAttribute("href");
      await driver.findElement(By.xpath(mainPageProductsFirstProductAddButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      itemsCount = await driver.findElement(By.xpath(headerBagIconCounter)).getAttribute("innerText");
      expect(itemsCount).to.equal('1');
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      href2 = await driver.findElement(By.xpath(cartDropdownFirstProductImg)).getAttribute("href");
      expect(href1 == href2).to.be.true;
      await driver.findElement(By.xpath(cartDropdownDeleteProductButton)).click();
    })

    it('Possible to add a product to the cart from the item page', async function () {
      await driver.get(`${URL}`);
      href1 = await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).getAttribute("href");
      await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).click();
      await driver.wait(until.elementLocated(By.xpath(productPageAddButton)), timeout);
      await driver.findElement(By.xpath(productPageAddButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      itemsCount = await driver.findElement(By.xpath(headerBagIconCounter)).getAttribute("innerText");
      expect(itemsCount).to.equal('1');
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      href2 = await driver.findElement(By.xpath(cartDropdownFirstProductImg)).getAttribute("href");
      expect(href1 == href2).to.be.true;
      await driver.findElement(By.xpath(cartDropdownDeleteProductButton)).click();
    })

    it('Possible to make an order', async function () {
      email2 = await randomNumber(1,3);
      await register(email2);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      href1 = await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).getAttribute("href");
      await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).click();
      await driver.wait(until.elementLocated(By.xpath(productPageAddButton)), timeout);
      price = await driver.findElement(By.xpath(productPrice)).getAttribute("innerText");
      await driver.findElement(By.xpath(productPageAddButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      await driver.findElement(By.xpath(cartDropdownOrderButton)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderName)), timeout);
      await driver.findElement(By.xpath(makeOrderName)).sendKeys('Максим');
      await driver.findElement(By.xpath(makeOrderSurName)).sendKeys('Алексеев');
      await driver.findElement(By.xpath(makeOrderZip)).sendKeys('12345');
      await driver.findElement(By.xpath(makeOrderAddress)).sendKeys('Москва, Красная площадь, д.1');
      await driver.findElement(By.xpath(makeOrderPhone)).sendKeys('+71234567890');
      await driver.findElement(By.xpath(makeOrderDeliveryDropdown)).click();
      deliveryExpected = await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).getAttribute("innerText");
      await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderNextButton)), timeout);
      await driver.findElement(By.xpath(makeOrderNextButton)).click();
      orderNumberOrdered = await driver.findElement(By.xpath(ordersFirstOrderNumber)).getAttribute("innerText");
      await driver.findElement(By.xpath(ordersFirstOrderNumber)).click();
      currentOrderURL = await driver.getCurrentUrl();
      await driver.wait(until.elementLocated(By.xpath(orderProductName)), timeout);
      href2 = await driver.findElement(By.xpath(orderProductNameLink)).getAttribute("href");
      totalAmount = await driver.findElement(By.xpath(orderSumme)).getAttribute("innerText");
      deliveryActual = await driver.findElement(By.xpath(orderDeliveryName)).getAttribute("innerText");
      expect(href1 == href2).to.be.true;
      expect(price.replace(' ₽', '') == totalAmount.replace(',00', '')).to.be.true;
      expect(deliveryActual).to.contain(deliveryExpected);
    })

    it('Possible to cancel an order', async function () {
      await driver.get(`${currentOrderURL}`);
      await driver.wait(until.elementLocated(By.xpath(orderCancelButton)), timeout);
      await driver.findElement(By.xpath(orderCancelButton)).click();
      await driver.wait(until.elementLocated(By.xpath(ordersFirstOrderNumber)), timeout);
      orderNumberCancelled = await driver.findElement(By.xpath(ordersFirstOrderNumber)).getAttribute("innerText");
      orderStatusCancelled = await driver.findElement(By.xpath(ordersFirstOrderStatus)).getAttribute("innerText");
      expect(orderNumberCancelled == orderNumberOrdered).to.be.true;
      expect(orderStatusCancelled).to.equal('Отменен');
    })

    it('Possible to make an order from the cart page', async function () {
      await driver.get(`${URL}`);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      href1 = await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).getAttribute("href");
      await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).click();
      await driver.wait(until.elementLocated(By.xpath(productPageAddButton)), timeout);
      price1 = await driver.findElement(By.xpath(productPrice)).getAttribute("innerText");
      await driver.findElement(By.xpath(productPageAddButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      await driver.findElement(By.xpath(cartDropdownCartButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      href2 = await driver.findElement(By.xpath(cartProductNameLink)).getAttribute("href");
      price2 = await driver.findElement(By.xpath(cartPrice)).getAttribute("innerText");
      expect(href1 == href2).to.be.true;
      expect(price1.replace(' ₽', '') == price2.replace('р', '')).to.be.true;
      await driver.wait(until.elementLocated(By.xpath(cartCheckoutButton)), timeout);
      await driver.findElement(By.xpath(cartCheckoutButton)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderName)), timeout);
      await driver.findElement(By.xpath(makeOrderName)).sendKeys('Максим');
      await driver.findElement(By.xpath(makeOrderSurName)).sendKeys('Алексеев');
      await driver.findElement(By.xpath(makeOrderZip)).sendKeys('12345');
      await driver.findElement(By.xpath(makeOrderAddress)).sendKeys('Москва, Красная площадь, д.1');
      await driver.findElement(By.xpath(makeOrderPhone)).sendKeys('+71234567890');
      await driver.findElement(By.xpath(makeOrderDeliveryDropdown)).click();
      deliveryExpected = await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).getAttribute("innerText");
      await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderNextButton)), timeout);
      await driver.findElement(By.xpath(makeOrderNextButton)).click();
      orderNumberOrdered = await driver.findElement(By.xpath(ordersFirstOrderNumber)).getAttribute("innerText");
      await driver.findElement(By.xpath(ordersFirstOrderNumber)).click();
      currentOrderURL = await driver.getCurrentUrl();
      await driver.wait(until.elementLocated(By.xpath(orderProductName)), timeout);
      href3 = await driver.findElement(By.xpath(orderProductNameLink)).getAttribute("href");
      totalAmount = await driver.findElement(By.xpath(orderSumme)).getAttribute("innerText");
      deliveryActual = await driver.findElement(By.xpath(orderDeliveryName)).getAttribute("innerText");
      expect(href1 == href3).to.be.true;
      expect(price1.replace(' ₽', '') == totalAmount.replace(',00', '')).to.be.true;
      expect(deliveryActual).to.contain(deliveryExpected);
    })

    after(() => driver && driver.quit())
  })

  describe('Smoking tests negative scenarios', function () {

    before(async function () {
      // env.builder() returns a Builder instance preconfigured for the
      // envrionment's target browser (you may still define browser specific
      // options if necessary (i.e. firefox.Options or chrome.Options)).
      driver = await env.builder().forBrowser(Browser.CHROME).setChromeOptions(options).build();
      driver.manage().window().maximize();
    })

    it('Shouldn\'t login unregistered', async function () {
      email3 = await randomNumber(1,3);
      await login(email3);
      alertText = await driver.findElement(By.xpath(authorizationAlert2)).getAttribute("innerText");
      expect(alertText).to.equal('Неправильный логин или пароль');
    })

    it('Shouldn\'t add more products than are in stock', async function () {
      await driver.get(`${URL}`);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).click();
      await driver.wait(until.elementLocated(By.xpath(productQuantityInput)), timeout);
      maxQuantity = await driver.findElement(By.xpath(productQuantityInput)).getAttribute("max");
      await driver.findElement(By.xpath(productQuantityInput)).clear();
      await driver.findElement(By.xpath(productQuantityInput)).sendKeys(Number(maxQuantity) + 1);
      await driver.wait(until.elementLocated(By.xpath(productPageAddButton)), timeout);
      await driver.findElement(By.xpath(productPageAddButton)).click();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      itemsCount = await driver.findElement(By.xpath(headerBagIconCounter)).getAttribute("innerText");
      expect(itemsCount).to.equal('0');
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      innerText = await driver.findElement(By.xpath(cartDropdownProductsWrapperPlaceholder)).getAttribute("innerText");
      expect(innerText).to.equal('Тут пока пусто');
    })

    after(() => driver && driver.quit())
  })

  describe('Bug fixes control', function () {

    before(async function () {
      // env.builder() returns a Builder instance preconfigured for the
      // envrionment's target browser (you may still define browser specific
      // options if necessary (i.e. firefox.Options or chrome.Options)).
      driver = await env.builder().forBrowser(Browser.CHROME).setChromeOptions(options).build();
      driver.manage().window().maximize();
    })

    it('Main page should opens after pseudo-user registration', async function () {
      email4 = await randomNumber(1,3);
      await driver.get(`${URL}`);
      await addProductFromMain();
      await register(email4);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      await logout();
    })

    it('Main page should opens after pseudo-user registration with empty cart', async function () {
      email5 = await randomNumber(1,3);
      await driver.get(`${URL}`);
      await addProductFromMain();
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      await driver.wait(until.elementLocated(By.xpath(cartDropdownDeleteProductButton)), timeout);
      await driver.findElement(By.xpath(cartDropdownDeleteProductButton)).click();
      await register(email5);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      await logout();
    })

    it('Possible to make an order after pseudo-user registration', async function () {
      email6 = await randomNumber(1,3);
      await driver.get(`${URL}`);
      await driver.wait(until.elementLocated(By.xpath(mainPageProductsFirstProductImg)), timeout);
      href1 = await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).getAttribute("href");
      await driver.findElement(By.xpath(mainPageProductsFirstProductImg)).click();
      await driver.wait(until.elementLocated(By.xpath(productPageAddButton)), timeout);
      price = await driver.findElement(By.xpath(productPrice)).getAttribute("innerText");
      await driver.findElement(By.xpath(productPageAddButton)).click();
      await register(email6);
      await driver.wait(until.elementLocated(By.xpath(headerBagIcon)), timeout);
      await driver.findElement(By.xpath(headerBagIcon)).click();
      await driver.sleep(sleepTimeout);
      await driver.findElement(By.xpath(cartDropdownOrderButton)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderName)), timeout);
      await driver.findElement(By.xpath(makeOrderName)).sendKeys('Максим');
      await driver.findElement(By.xpath(makeOrderSurName)).sendKeys('Алексеев');
      await driver.findElement(By.xpath(makeOrderZip)).sendKeys('12345');
      await driver.findElement(By.xpath(makeOrderAddress)).sendKeys('Москва, Красная площадь, д.1');
      await driver.findElement(By.xpath(makeOrderPhone)).sendKeys('+71234567890');
      await driver.findElement(By.xpath(makeOrderDeliveryDropdown)).click();
      deliveryExpected = await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).getAttribute("innerText");
      await driver.findElement(By.xpath(makeOrderDeliveryDropdownCDEK)).click();
      await driver.wait(until.elementLocated(By.xpath(makeOrderNextButton)), timeout);
      await driver.findElement(By.xpath(makeOrderNextButton)).click();
      orderNumberOrdered = await driver.findElement(By.xpath(ordersFirstOrderNumber)).getAttribute("innerText");
      await driver.findElement(By.xpath(ordersFirstOrderNumber)).click();
      currentOrderURL = await driver.getCurrentUrl();
      await driver.wait(until.elementLocated(By.xpath(orderProductName)), timeout);
      href2 = await driver.findElement(By.xpath(orderProductNameLink)).getAttribute("href");
      totalAmount = await driver.findElement(By.xpath(orderSumme)).getAttribute("innerText");
      deliveryActual = await driver.findElement(By.xpath(orderDeliveryName)).getAttribute("innerText");
      expect(href1 == href2).to.be.true;
      expect(price.replace(' ₽', '') == totalAmount.replace(',00', '')).to.be.true;
      expect(deliveryActual).to.contain(deliveryExpected);
      await logout();
    })

    after(() => driver && driver.quit())
  })
})